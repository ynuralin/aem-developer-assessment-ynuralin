package au.edu.sydney.contacts.core.models;

import com.adobe.cq.sightly.WCMUse;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

/**
 * Created by user on 7/28/16.
 */
public class ContactComponent extends WCMUse {

    private Contact contact;

    @Override
    public void activate() throws Exception {
        Resource currentResource = getResource();
        ValueMap properties = currentResource.adaptTo(ValueMap.class);

        contact = buildContact(properties);
    }

    public Contact getContact() {
        return contact;
    }

    private Contact buildContact(ValueMap properties) {
        Contact contact = new Contact();
        String contactPath = properties.get("cq:contactPage", (String) null);
        if(contactPath == null) {
            contact.setMessage("null");
            return contact;
        }

        PageManager pageManager = getPageManager();
        Page contactPage = pageManager.getPage(contactPath);
        if(contactPage == null) {
            contact.setMessage("notpage");
            return contact;
        }

        String primaryType = contactPage.getProperties().get("jcr:primaryType", (String) null);
        if(!"cq:PageContent".equals(primaryType)) {
            contact.setMessage("notpage");
            return contact;
        }

        Resource contactResource = getResourceResolver().getResource(contactPage.getPath() + "/jcr:content/profile");
        ValueMap contactProperties = contactResource.adaptTo(ValueMap.class);

        contact.setPath(contactPath);
        contact.setTitle(contactProperties.get("title", (String) null));
        contact.setName(contactProperties.get("name", (String) null));
        contact.setPosition(contactProperties.get("position", (String) null));
        contact.setDepartment(contactProperties.get("department", (String) null));
        contact.setPhone(contactProperties.get("phone", (String[]) null));
        contact.setEmail(contactProperties.get("email", (String[]) null));

        return contact;
    }
}


