package au.edu.sydney.contacts.core.models;

import com.adobe.cq.sightly.WCMUse;
import io.wcm.testing.mock.aem.junit.AemContext;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by user on 7/28/16.
 */

public class TestContactComponent {

    @Rule
    public final AemContext context = new AemContext(ResourceResolverType.JCR_MOCK);

    private ContactComponent contactComponent;

    @Before
    public void setup() throws Exception {
        context.load().json("/home.json", "/content/contacts/home");
        context.load().json("/d-jessup.json", "/content/contacts/people/d/d-jessup");

        context.currentResource("/content/contacts/home/jcr:content/par/contactcard");

        contactComponent = spy(ContactComponent.class);
        doReturn(context.currentResource()).when(contactComponent).getResource();
        doReturn(context.pageManager()).when(contactComponent).getPageManager();
        doReturn(context.resourceResolver()).when(contactComponent).getResourceResolver();
    }

    @Test
    public void testGetContact() throws Exception {
        contactComponent.activate();
        Contact contact = contactComponent.getContact();
        Assert.assertNotNull(contact);

        Assert.assertEquals(contact.getTitle(), "");
        Assert.assertEquals(contact.getName(), "David Jessup");
        Assert.assertEquals(contact.getPosition(), "Web and Digital Development Manager");
        Assert.assertEquals(contact.getDepartment(), "Web and Digital Development");
        Assert.assertNull(contact.getMessage());
        Assert.assertArrayEquals(contact.getPhone(), new String[] {"+61 2 9351 5508"});
        Assert.assertArrayEquals(contact.getEmail(), new String[] {"david.jessup@sydney.edu.au"});
    }
}
